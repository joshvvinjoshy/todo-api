const express = require("express");
const userRouter = express.Router();
const uuid = require("uuid");
const mysql = require("mysql");
const { connection } = require("../../database/databaseConnection.js");

//get all the users
userRouter.get("/", (req, res) => {
  const query = `SELECT * FROM user`;
  connection.query(query, (err, data) => {
    try {
      if (err) {
        throw new Error(`Error retrieving users`);
      } else {
        if (data.length == 0) {
          throw new Error(`Error retrieving users`);
        }
        res.json(data);
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  });
});

//get the details of a particular user
userRouter.get("/:username", (req, res) => {
  const query = `SELECT * FROM user WHERE username = '${req.params.username}'`;
  connection.query(query, (err, data) => {
    try {
      if (err) {
        throw new Error(
          `Error retrieving details of the user with username ${req.params.username}`
        );
      } else {
        if (data.length == 0) {
          throw new Error(
            `Error retrieving details of the user with username ${req.params.username}`
          );
        }
        res.json(data);
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  });
});

// create a new user
userRouter.post("/", (req, res) => {
  const newUser = {
    username: uuid.v4(),
    name: req.body.name,
  };
  const query = `INSERT INTO user VALUES ('${newUser.username}', '${newUser.name}')`;
  connection.query(query, (err, data) => {
    try {
      if (err) {
        throw new Error(`Failed to create new user`);
      } else {
        res.json({ message: "new user added", newUser });
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  });
});

module.exports = userRouter;
