const express = require("express");
const projectRouter = express.Router();
const uuid = require("uuid");
const mysql = require("mysql");
const { connection } = require("../../database/databaseConnection.js");

// create a new project
projectRouter.post("/", (req, res) => {
  const newProject = {
    id: uuid.v4(),
    title: req.body.title,
    description: req.body.description,
    username: req.body.username,
  };
  const query = `INSERT INTO project VALUES ('${newProject.id}', '${newProject.title}', '${newProject.description}', '${newProject.username}')`;
  //   console.log(query);
  connection.query(query, (err, data) => {
    try {
      if (err) {
        throw new Error(`Failed to create new project`);
      } else {
        res.json({ message: "new project added", newProject });
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  });
});

//get all projects
projectRouter.get("/", (req, res) => {
  const query = `SELECT * FROM project`;
  connection.query(query, (err, data) => {
    try {
      if (err) {
        throw new Error(`Error retrieving projects`);
      } else {
        if (data.length == 0) {
          throw new Error(`Error retrieving projects`);
        }
        res.json(data);
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  });
});

//get the details of a particular project
projectRouter.get("/:id", (req, res) => {
  const query = `SELECT * FROM project WHERE id = '${req.params.id}'`;
  connection.query(query, (err, data) => {
    try {
      if (err) {
        throw new Error(
          `Error retrieving details of the project with id ${req.params.id}`
        );
      } else {
        if (data.length == 0) {
          throw new Error(
            `Error retrieving details of the project with id ${req.params.id}`
          );
        }
        res.json(data);
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  });
});

// update individual project
projectRouter.put("/:id", (req, res) => {
  const project_details = [];
  if (req.body.title) {
    project_details.push(`title = '${req.body.title}'`);
  }
  if (req.body.description) {
    project_details.push(`description = '${req.body.description}'`);
  }
  if (req.body.username) {
    project_details.push(`username = '${req.body.username}'`);
  }
  const query = `UPDATE project SET ${project_details.join(",")} WHERE id = '${
    req.params.id
  }'`;
  connection.query(query, (err, data) => {
    try {
      if (err) {
        throw new Error(`Failed to update user with id ${req.params.id}`);
      } else {
        res.json({ message: "project updated" });
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  });
});

// delete a particular user
projectRouter.delete("/:id", (req, res) => {
  const query = `DELETE FROM project WHERE id = '${req.params.id}'`;
  connection.query(query, (err, data) => {
    try {
      if (err) {
        throw new Error(`Failed to delete project with id ${req.params.id}`);
      } else {
        res.json({ message: "project deleted" });
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  });
});

module.exports = projectRouter;
