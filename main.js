const express = require("express");
const app = express();
const logger = require("morgan");
const userRouter = require("./routes/user/users.js");
const projectRouter = require("./routes/project/project.js");

const port = process.env.PORT || 8000;

app.use(logger("dev"));
app.use(express.json());

app.use("/api/user", userRouter);
app.use("/api/project", projectRouter);

app.listen(port, () => {
  console.log(`Server started listening on ${port}`);
});
