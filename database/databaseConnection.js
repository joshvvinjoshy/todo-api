const mysql = require("mysql");
require("dotenv").config();
const connection = mysql.createConnection({
  host: "localhost",
  user: process.env.VITE_USERNAME,
  password: process.env.VITE_PASSWORD,
  database: "crud_db",
});
connection.connect((error) => {
  if (error) {
    console.error(error);
  } else {
    console.log("Connected to the database");
  }
});
module.exports = { connection };
